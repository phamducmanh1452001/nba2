package controller;

import au.edu.uts.ap.javafx.Controller;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ErrorController extends Controller<String> {

    @FXML
    private Label errorLabel;

    @FXML
    private Button okButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private  void setLabels() {
        errorLabel.setText(this.model);
        okButton.setText("Okay");
    }

    private  void setActions() {
        okButton.setOnAction(event -> {
            final Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
        });
    }
}
