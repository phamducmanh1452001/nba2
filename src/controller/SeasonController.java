package controller;

import au.edu.uts.ap.javafx.Controller;
import au.edu.uts.ap.javafx.ViewLoader;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Season;
import view.assets.FXMLPath;
import view.assets.ImagePath;

import java.net.URL;
import java.util.ResourceBundle;

public class SeasonController extends Controller<Season> {

    @FXML
    private ImageView backgroundImageView;

    @FXML
    private Button addTeamToRoundButton;

    @FXML
    private Button currentRoundButton;

    @FXML
    private Button playGameButton;

    @FXML
    private Button gameResultRecordButton;

    @FXML
    private Button closeButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private void setLabels() {
        addTeamToRoundButton.setText("Round");
        currentRoundButton.setText("Current");
        playGameButton.setText("Game");
        gameResultRecordButton.setText("Result");
        closeButton.setText("Close");

        final Image image = new Image(getClass().getResourceAsStream(ImagePath.nbaBackground));
        backgroundImageView.setImage(image);
    }

    private void setActions() {
        currentRoundButton.setOnAction(event ->  {
            ViewLoader.showNewStage(
                    FXMLPath.currentRoundTeamsView,
                    "Tournament",
                    this.model
            );
        });
        addTeamToRoundButton.setOnAction(event -> {
            ViewLoader.showNewStage(
                    FXMLPath.seasonRoundView,
                    "Season Rounds",
                    new Season()
            );
        });

        closeButton.setOnAction(event -> {
            closeStage();
        });
    }

    private void closeStage() {
        final Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
