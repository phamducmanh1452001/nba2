package au.edu.uts.ap.javafx;

import javafx.fxml.Initializable;
import javafx.stage.Stage;

public abstract class Controller<M> implements Initializable {

    protected M model;
    protected Stage stage;
}
