import au.edu.uts.ap.javafx.ViewLoader;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Association;
import view.assets.FXMLPath;

public class NBAfxApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("NBAfxApp");

        ViewLoader.showStage(FXMLPath.associationView, primaryStage, new Association());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
